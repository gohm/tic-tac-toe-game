#!/usr/bin/python3

from time import sleep

player_1_sign = " "
player_2_sign = " "
players_assigment = False
are_you_ready_to_start = False
winner = "noone"

game_board_data = {
    "c1":"1","c2":"2","c3":"3","c4":"4","c5":"5","c6":"6","c7":"7","c8":"8","c9":"9"
}

horizontal_separator = "-----"

def introduction_screen():
    print("Hello in Tic Tac Toe game")

def chose_x_or_o():
    while not players_assigment:
        print("Player 1 pls choose sign x or o?")
        player_1_sign = input()
        if player_1_sign == "x" or player_1_sign == "o":
            return player_1_sign
        else:
            print("Wrong sign. Player 1 pls choose sign x or o?")
        
def assign_sign_for_player_2():
    if player_1_sign == "x":
        player_2_sign = "o"
    else:
        player_2_sign = "x"
    return player_2_sign

def ready_to_start():
    print("Player 1: " + player_1_sign + " Player 2: " + player_2_sign)
    print('Are you ready to start? Type: "yes" or "not"')
    are_you_ready_to_start = input()
    if are_you_ready_to_start == "yes" or are_you_ready_to_start == "y":
        return True
    else:
        return False

def print_game_board():
    for l in range(1,6):
        if l == 1:
            print(game_board_data.get("c7") + "|" + game_board_data.get("c8") + "|" + game_board_data.get("c9"))
        elif l == 2 or l == 4:
            print(horizontal_separator)
        elif l == 3:
            print(game_board_data.get("c4") + "|" + game_board_data.get("c5") + "|" + game_board_data.get("c6"))
        elif l == 5:
            print(game_board_data.get("c1") + "|" + game_board_data.get("c2") + "|" + game_board_data.get("c3"))

def clean_game_board():
    return {key: " " for key in game_board_data}

def move(player):
    if player == 1:
        print("Player 1 pls type 1-9 for choose move")
        player_1_move = input()
        return player_1_move
    else:
        print("Player 2 pls type 1-9 for choose move")
        player_2_move = input()
        return player_2_move

def update_game_board_data(player, move, sign):
    if player == 1:
        game_board_data["c" + str(move)] = sign
    else:
        game_board_data["c" + str(move)] = sign

def check_allow_to_move(move):
    return game_board_data["c" + str(move)] == " "

def check_winner():
    if game_board_data["c1"] == game_board_data["c2"] and game_board_data["c1"] == game_board_data["c3"] and game_board_data["c1"] == player_1_sign:
        return "player_1"
    elif game_board_data["c4"] == game_board_data["c5"] and game_board_data["c4"] == game_board_data["c6"] and game_board_data["c4"] == player_1_sign:
        return "player_1"
    elif game_board_data["c7"] == game_board_data["c8"] and game_board_data["c7"] == game_board_data["c9"] and game_board_data["c7"] == player_1_sign:
        return "player_1"
    elif game_board_data["c1"] == game_board_data["c5"] and game_board_data["c1"] == game_board_data["c9"] and game_board_data["c1"] == player_1_sign:
        return "player_1"
    elif game_board_data["c7"] == game_board_data["c5"] and game_board_data["c7"] == game_board_data["c3"] and game_board_data["c7"] == player_1_sign:
        return "player_1"
    elif game_board_data["c1"] == game_board_data["c4"] and game_board_data["c1"] == game_board_data["c7"] and game_board_data["c1"] == player_1_sign:
        return "player_1"
    elif game_board_data["c2"] == game_board_data["c5"] and game_board_data["c2"] == game_board_data["c8"] and game_board_data["c2"] == player_1_sign:
        return "player_1"
    elif game_board_data["c3"] == game_board_data["c6"] and game_board_data["c3"] == game_board_data["c9"] and game_board_data["c3"] == player_1_sign:
        return "player_1"
    
    elif game_board_data["c1"] == game_board_data["c2"] and game_board_data["c1"] == game_board_data["c3"] and game_board_data["c1"] == player_2_sign:
        return "player_2"
    elif game_board_data["c4"] == game_board_data["c5"] and game_board_data["c4"] == game_board_data["c6"] and game_board_data["c4"] == player_2_sign:
        return "player_2"
    elif game_board_data["c7"] == game_board_data["c8"] and game_board_data["c7"] == game_board_data["c9"] and game_board_data["c7"] == player_2_sign:
        return "player_2"
    elif game_board_data["c1"] == game_board_data["c5"] and game_board_data["c1"] == game_board_data["c9"] and game_board_data["c1"] == player_2_sign:
        return "player_2"
    elif game_board_data["c7"] == game_board_data["c5"] and game_board_data["c7"] == game_board_data["c3"] and game_board_data["c7"] == player_2_sign:
        return "player_2"
    elif game_board_data["c1"] == game_board_data["c4"] and game_board_data["c1"] == game_board_data["c7"] and game_board_data["c1"] == player_2_sign:
        return "player_2"
    elif game_board_data["c2"] == game_board_data["c5"] and game_board_data["c2"] == game_board_data["c8"] and game_board_data["c2"] == player_2_sign:
        return "player_2"
    elif game_board_data["c3"] == game_board_data["c6"] and game_board_data["c3"] == game_board_data["c9"] and game_board_data["c3"] == player_2_sign:
        return "player_2"
    else:
        return "noone"

def winner_print():
    print("Congratulation " + winner + " won!!.")

introduction_screen()
player_1_sign = chose_x_or_o()
player_2_sign = assign_sign_for_player_2()

are_you_ready_to_start = ready_to_start()
if are_you_ready_to_start:
    for x in range(0,2):
        print(".")
        sleep(0.5)
else:
    print("end")

print("How to play")
print_game_board()
game_board_data = clean_game_board()
print("cleare data")
print_game_board()


for m in range(1,10):
    if m % 2 != 0:
        player_1_move = move(1)
        if check_allow_to_move(player_1_move):
            update_game_board_data(1,player_1_move,player_1_sign)
            print_game_board()
            if check_winner() == "player_1" or check_winner() == "player_2":
                break
        else:
            print("jeszcze raz")
            player_1_move = move(1)

    elif m % 2 == 0:
        player_2_move = move(2)
        if check_allow_to_move(player_2_move):
            update_game_board_data(2,player_2_move,player_2_sign)
            print_game_board()
            if check_winner() == "player_1" or check_winner() == "player_2":
                break
        else:
            print("jeszcze raz")
            player_2_move = move(2)
winner = check_winner()
winner_print()
